/*
    Sumo|LIB - A library for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|LIB.

    Sumo|LIB is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|LIB (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file sumouy.h
/// \brief Sumo|LIB header file.
/// \details This is the file for use in the Sumo|UY competition.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <SDL_net.h>

//===========================================================//
//Macros                                                     //
//===========================================================//

////////////////////////////////////////////////////
/// \def SUMOUY_VERSION
/// \brief Sumo|LIB version.
////////////////////////////////////////////////////
#define SUMOUY_VERSION 1

////////////////////////////////////////////////////
/// \def SUMOUY_VENDOR
/// \brief Sumo|LIB distributor.
////////////////////////////////////////////////////
#define SUMOUY_VENDOR 2

////////////////////////////////////////////////////
/// \def SUMOUY_COMPILE_DATE
/// \brief Sumo|LIB compile date.
////////////////////////////////////////////////////
#define SUMOUY_COMPILE_DATE 3

////////////////////////////////////////////////////
/// \def SUMOUY_TRUE
/// \brief Sumo|LIB boolean true.
////////////////////////////////////////////////////
#define SUMOUY_TRUE 1

////////////////////////////////////////////////////
/// \def SUMOUY_FALSE
/// \brief Sumo|LIB boolean false.
////////////////////////////////////////////////////
#define SUMOUY_FALSE 0

//===========================================================//
//Types                                                      //
//===========================================================//

////////////////////////////////////////////////////
/// \typedef guint8 SumoUY_UInt8
/// \brief The Sumo|UY unsigned char.
////////////////////////////////////////////////////
typedef guint8 SumoUY_UInt8;

////////////////////////////////////////////////////
/// \typedef gint8 SumoUY_Int8
/// \brief The Sumo|UY signed char.
////////////////////////////////////////////////////
typedef gint8 SumoUY_Int8;

////////////////////////////////////////////////////
/// \typedef guint16 SumoUY_UInt16
/// \brief The Sumo|UY unsigned short.
////////////////////////////////////////////////////
typedef guint16 SumoUY_UInt16;

////////////////////////////////////////////////////
/// \typedef gint16 SumoUY_Int16
/// \brief The Sumo|UY signed short.
////////////////////////////////////////////////////
typedef gint16 SumoUY_Int16;

////////////////////////////////////////////////////
/// \typedef guint32 SumoUY_UInt32
/// \brief The Sumo|UY unsigned int.
////////////////////////////////////////////////////
typedef guint32 SumoUY_UInt32;

////////////////////////////////////////////////////
/// \typedef gint32 SumoUY_Int32
/// \brief The Sumo|UY signed int.
////////////////////////////////////////////////////
typedef gint32 SumoUY_Int32;

////////////////////////////////////////////////////
/// \typedef guint64 SumoUY_UInt64
/// \brief The Sumo|UY unsigned long long.
////////////////////////////////////////////////////
typedef guint64 SumoUY_UInt64;

////////////////////////////////////////////////////
/// \typedef gint64 SumoUY_Int64
/// \brief The Sumo|UY signed long long.
////////////////////////////////////////////////////
typedef gint64 SumoUY_Int64;

////////////////////////////////////////////////////
/// \typedef gfloat SumoUY_Float
/// \brief The Sumo|UY floating point.
////////////////////////////////////////////////////
typedef gfloat SumoUY_Float;

////////////////////////////////////////////////////
/// \typedef gdouble SumoUY_Double
/// \brief The Sumo|UY double.
////////////////////////////////////////////////////
typedef gdouble SumoUY_Double;

//===========================================================//
//Enumerations                                               //
//===========================================================//

////////////////////////////////////////////////////
/// \enum SumoUYError
/// \brief Enumeration to determine error type.
////////////////////////////////////////////////////
enum SumoUYError
{
	SUMOUY_NO_ERROR,
	SUMOUY_CANT_CREATE_BOT,
	SUMOUY_COULD_NOT_INIT,
	SUMOUY_INVALID_ARGUMENT,
	SUMOUY_CANT_RECIEVE_DATA,
	SUMOUY_CANT_SEND_DATA,
	SUMOUY_CANT_CREATE_SOCKET,
	SUMOUY_CANT_RESOLVE_HOST,
	SUMOUY_CANT_SET_SPEED,
	SUMOUY_CANT_PROCESS_DATA,
	SUMOUY_INVALID_DATA,
	SUMOUY_NOT_INITIALIZED,
	SUMOUY_NO_PACKET_TO_RECIEVE,
	SUMOUY_CANT_CREATE_CONTROLLER
};

////////////////////////////////////////////////////
/// \enum SumoUYFightState
/// \brief Enumeration to determine the fight state.
////////////////////////////////////////////////////
enum SumoUYFightState
{
	SUMOUY_STARTED = 0,
	SUMOUY_STOPPED = 1,
	SUMOUY_FINISHED = 2,
	SUMOUY_TESTING = 3
};

////////////////////////////////////////////////////
/// \enum SumoUYWinner
/// \brief Enumeration to determine the fight winner.
////////////////////////////////////////////////////
enum SumoUYWinner
{
	SUMOUY_ROBOT1 = 0,
	SUMOUY_ROBOT2 = 1,
	SUMOUY_EQUALITY = 2,
	SUMOUY_NOT_DEFINED = 3
};

////////////////////////////////////////////////////
/// \enum SumoUYServerComponent
/// \brief Enumeration to determine a server component.
////////////////////////////////////////////////////
enum SumoUYServerComponent
{
	SUMOUY_VIEWER = 0,
	SUMOUY_BOT_ONE = 1,
	SUMOUY_BOT_TWO = 2
};

enum SumoUYBots
{
    SUMOUY_BOT1 = 1,
    SUMOUY_BOT2 = 2
};

////////////////////////////////////////////////////
/// \enum SumoUYSize
/// \brief Enumeration to determine a size.
////////////////////////////////////////////////////
enum SumoUYSize
{
        SUMOUY_EQUAL = 0,
        SUMOUY_BIGGER = 1,
        SUMOUY_SMALLER = 2
};

//===========================================================//
//Structures                                                 //
//===========================================================//

////////////////////////////////////////////////////
/// \struct Vector2
/// \brief Integer-type vector of 2 dimensions.
////////////////////////////////////////////////////
struct Vector2
{
    SumoUY_Int32 X;
    SumoUY_Int32 Y;
};

////////////////////////////////////////////////////
/// \struct Vector3
/// \brief Integer-type vector of 3 dimensions.
////////////////////////////////////////////////////
struct Vector3
{
	SumoUY_Int32 X;
	SumoUY_Int32 Y;
	SumoUY_Int32 Z;
};

////////////////////////////////////////////////////
/// \struct SpaceRotation
/// \brief Spatial rotation in 3 axis.
////////////////////////////////////////////////////
struct SpaceRotation
{
	SumoUY_UInt16 X;
	SumoUY_UInt16 Y;
	SumoUY_UInt16 Z;
};

////////////////////////////////////////////////////
/// \struct BotSpeed
/// \brief Speed structure.
////////////////////////////////////////////////////
struct BotSpeed
{
    SumoUY_Int8 LeftWheelSpeed;
    SumoUY_Int8 RightWheelSpeed;
};

////////////////////////////////////////////////////
/// \struct NetworkAddress
/// \brief Network address with host and port.
////////////////////////////////////////////////////
struct NetworkAddress
{
	char *Host;
	SumoUY_UInt16 Port;
};

////////////////////////////////////////////////////
/// \struct FightTime
/// \brief Fight time structure.
////////////////////////////////////////////////////
struct FightTime
{
	SumoUY_UInt8 Hours;
	SumoUY_UInt8 Minutes;
	SumoUY_UInt8 Seconds;
};

////////////////////////////////////////////////////
/// \struct SumoBot
/// \brief Basic bot structure.
////////////////////////////////////////////////////
struct SumoBot
{
	struct NetworkAddress ServerAddress;
	struct NetworkAddress BotAddress;
	void *Socket;
	void *AddressData;
	struct Vector2 Position;
	SumoUY_UInt16 Rotation;
	SumoUY_UInt8 Yukos;
	struct BotSpeed Speed;
	struct Vector2 OpponentPosition;
	SumoUY_UInt16 OpponentRotation;
	SumoUY_UInt8 OpponentYukos;
	void (*OnStartFight)();
	void (*OnStopFight)();
	void (*OnReposition)(struct Vector2 position, SumoUY_UInt16 rotation);
	void (*OnUpdate)(struct Vector2 position,SumoUY_UInt16 rotation, SumoUY_UInt8 yukos, struct Vector2 opponentPosition, SumoUY_UInt16 opponentRotation, SumoUY_UInt8 opponentYukos);
};

////////////////////////////////////////////////////
/// \struct SumoServer
/// \brief Basic server structure.
////////////////////////////////////////////////////
struct SumoServer
{
	struct NetworkAddress ViewerAddress;
	struct NetworkAddress ControllerAddress;
	struct NetworkAddress Bot1Address;
	struct NetworkAddress Bot2Address;
	struct NetworkAddress BotListen1Address;
	struct NetworkAddress BotListen2Address;
	void *Sockets;
	void *AddressData;
	struct Vector2 Bot1Position;
	struct Vector2 Bot2Position;
	SumoUY_UInt16 Bot1Rotation;
	SumoUY_UInt16 Bot2Rotation;
	struct BotSpeed Bot1Speed;
	struct BotSpeed Bot2Speed;
	SumoUY_UInt8 Bot1Yukos;
	SumoUY_UInt8 Bot2Yukos;
	SumoUY_UInt8 HasBot1ToResponseRequest;
	SumoUY_UInt8 HasBot2ToResponseRequest;
	SumoUY_UInt8 HasBot1RespondedRequest;
	SumoUY_UInt8 HasBot2RespondedRequest;
	struct FightTime FightTimeLimit;
	struct FightTime FightTimeElapsed;
	SumoUY_UInt8 FightState;
	SumoUY_UInt8 MaxRounds;
	SumoUY_UInt8 CurrentRound;
	void (*OnFightStateChanged)(SumoUY_UInt8 state);
};

////////////////////////////////////////////////////
/// \struct SumoController
/// \brief Basic controller structure.
////////////////////////////////////////////////////
struct SumoController
{
	struct NetworkAddress ServerAddress;
	struct NetworkAddress ControllerAddress;
	void *Socket;
	void *AddressData;
	struct Vector3 Bot1Position;
	struct Vector3 Bot2Position;
	struct SpaceRotation Bot1Rotation;
	struct SpaceRotation Bot2Rotation;
	void (*OnBotSpeedSet)(SumoUY_UInt8 bot, struct BotSpeed speed);
};

//===========================================================//
//Functions                                                  //
//===========================================================//

//******************************************************//
//General library functions                             //
//******************************************************//

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_Init()
/// \brief Initializes the Sumo|LIB library.
/// \details Sets the SUMOUY_COULD_NOT_INIT error flag if fails.
/// \return SUMOUY_TRUE if initialized or SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_Init();

////////////////////////////////////////////////////
/// \fn void SumoUY_Close()
/// \brief Closes the Sumo|LIB library and frees the components loaded.
////////////////////////////////////////////////////
void SumoUY_Close();

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_WasInit()
/// \brief Verifies the initialization state of the Sumo|LIB library.
/// \return SUMOUY_TRUE if initialized or SUMOUY_FALSE if not.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_WasInit();

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_GetError()
/// \brief Gets the last error ocurred.
/// \return The error type that has ocurred.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_GetError();

////////////////////////////////////////////////////
/// \fn char *SumoUY_GetErrorString()
/// \brief Gets the last error string.
/// \return The error string that has ocurred.
////////////////////////////////////////////////////
char *SumoUY_GetErrorString();

////////////////////////////////////////////////////
/// \fn void SumoUY_SetError(SumoUY_UInt8 error)
/// \brief Sets an error to the Sumo|UY library.
/// \param error The error to set.
////////////////////////////////////////////////////
void SumoUY_SetError(SumoUY_UInt8 error);

////////////////////////////////////////////////////
/// \fn void SumoUY_ClearErrors()
/// \brief Clears the errors in the Sumo|UY library.
////////////////////////////////////////////////////
void SumoUY_ClearErrors();

////////////////////////////////////////////////////
/// \fn const char *SumoUY_GetString(SumoUY_UInt8 attribute)
/// \brief Gets the specified attribute.
/// \param attribute The attribute specified to know about.
/// \return A string containing the information of the attribute specified or NULL if the attribute is invalid.
////////////////////////////////////////////////////
const char *SumoUY_GetString(SumoUY_UInt8 attribute);

//***************************************************************************************//
//Bot-related functions                                                                  //
//***************************************************************************************//

////////////////////////////////////////////////////
/// \fn struct SumoBot *SumoUY_CreateSumoBot(struct NetworkAddress serverAddress, struct NetworkAddress botAddress)
/// \brief Creates a new bot.
/// \details If fails sets the SUMOUY_CANT_CREATE_BOT error flag.
/// \param serverAddress The server address to connect.
/// \param botAddress The address of the server created by the bot.
/// \return The SumoBot created or NULL if fails.
////////////////////////////////////////////////////
struct SumoBot *SumoUY_CreateSumoBot(struct NetworkAddress serverAddress, struct NetworkAddress botAddress);

////////////////////////////////////////////////////
/// \fn void SumoUY_DestroySumoBot(struct SumoBot *bot)
/// \brief Destroys a SumoBot and frees its resources.
/// \details If fails sets the SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
////////////////////////////////////////////////////
void SumoUY_DestroySumoBot(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn void SumoUY_BotSendOK(struct SumoBot *bot)
/// \brief Sends the ok* response to the Sumo|SERVER.
/// \details If fails sets the SUMOUY_CANT_SEND_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotSendOK(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn void SumoUY_BotProcessData(struct SumoBot *bot)
/// \brief Process specified bot data and obtains the position and other values from the Sumo|SERVER.
/// \details If fails sets the SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE if can't process the data.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotProcessData(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn struct Vector2 SumoUY_BotGetPosition(struct SumoBot *bot)
/// \brief Gets the specified bot position.
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return The position vector.
////////////////////////////////////////////////////
struct Vector2 SumoUY_BotGetPosition(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt16 SumoUY_BotGetRotation(struct SumoBot *bot)
/// \brief Gets the specified bot rotation in degrees.
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return The rotation in degrees.
////////////////////////////////////////////////////
SumoUY_UInt16 SumoUY_BotGetRotation(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn struct BotSpeed SumoUY_BotGetSpeed(struct SumoBot *bot)
/// \brief Gets the specified bot speed.
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return The speed of the bot.
////////////////////////////////////////////////////
struct BotSpeed SumoUY_BotGetSpeed(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotGetYukos(struct SumoBot *bot)
/// \brief Gets the specified bot yukos (points).
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return The yukos of the bot.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotGetYukos(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn struct Vector2 SumoUY_BotGetOpponentPosition(struct SumoBot *bot)
/// \brief Gets the opponent bot position.
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return The opponent bot position vector.
////////////////////////////////////////////////////
struct Vector2 SumoUY_BotGetOpponentPosition(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt16 SumoUY_BotGetOpponentRotation(struct SumoBot *bot)
/// \brief Gets the opponent bot rotation.
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return The opponent bot rotation.
////////////////////////////////////////////////////
SumoUY_UInt16 SumoUY_BotGetOpponentRotation(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotGetOpponentYukos(struct SumoBot *bot)
/// \brief Gets the opponent bot yukos (points).
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \return The opponent bot yukos.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotGetOpponentYukos(struct SumoBot *bot);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotSetSpeed(struct SumoBot *bot, struct BotSpeed speed)
/// \brief Set the current bot speed. Values must be between -5 and 5.
/// \details If fails sets the SUMOUY_CANT_SEND_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \param speed The bot speed to set.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotSetSpeed(struct SumoBot *bot, struct BotSpeed speed);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotOnStartFight(struct SumoBot *bot, void (*onStartFight)())
/// \brief Specifies the method to call when the fight starts.
/// \details If fails sets the SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \param onStartFight The start function to call when the fight starts.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotOnStartFight(struct SumoBot *bot, void (*onStartFight)());

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotOnStopFight(struct SumoBot *bot, void (*onStopFight)())
/// \brief Specifies the method to call when the fight ends.
/// \details If fails sets the SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \param onStopFight The start function to call when the fight stops.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotOnStopFight(struct SumoBot *bot, void (*onStopFight)());

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotOnUpdate(struct SumoBot *bot, void (*onUpdate)(struct Vector2 position,SumoUY_UInt16 rotation, SumoUY_UInt8 yukos, struct Vector2 opponentPosition, SumoUY_UInt16 opponentRotation, SumoUY_UInt8 opponentYukos))
/// \brief Specifies the method to call when the Sumo|SERVER sends the data updated.
/// \details If fails sets the SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \param onUpdate The update function to call when the Sumo|SERVER sends the updated data.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotOnUpdate(struct SumoBot *bot, void (*onUpdate)(struct Vector2 position,SumoUY_UInt16 rotation, SumoUY_UInt8 yukos, struct Vector2 opponentPosition, SumoUY_UInt16 opponentRotation, SumoUY_UInt8 opponentYukos));

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotOnReposition(struct SumoBot *bot, void (*onReposition)(struct Vector2 position, SumoUY_UInt16 rotation))
/// \brief Specifies the method to call when the Sumo|SERVER sends reposition data.
/// \details If fails sets the SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \param onReposition The reposition function to call when the Sumo|SERVER sends the reposition data.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotOnReposition(struct SumoBot *bot, void (*onReposition)(struct Vector2 position, SumoUY_UInt16 rotation));

//***************************************************************************************//
//Controller-related functions                                                           //
//***************************************************************************************//

////////////////////////////////////////////////////
/// \fn struct SumoController *SumoUY_CreateSumoController(struct NetworkAddress serverAddress, struct NetworkAddress controllerAddress)
/// \brief Creates a new controller.
/// \param serverAddress The Sumo|SERVER address.
/// \param controllerAddress The controller address.
/// \return The created controller.
////////////////////////////////////////////////////
struct SumoController *SumoUY_CreateSumoController(struct NetworkAddress serverAddress, struct NetworkAddress controllerAddress);

////////////////////////////////////////////////////
/// \fn void SumoUY_DestroySumoController(struct SumoController *controller)
/// \brief Destroys a controller, freeing its resources.
/// \param controller The controller to destroy.
////////////////////////////////////////////////////
void SumoUY_DestroySumoController(struct SumoController *controller);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerProcessData(struct SumoController *controller)
/// \brief Process specified controller data and sends and obtains the values from the Sumo|SERVER.
/// \param *controller The Controller to use to do this function.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE if can't process the data.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerProcessData(struct SumoController *controller);

////////////////////////////////////////////////////
/// \fn struct Vector3 SumoUY_ControllerGetBot1Position(struct SumoController *controller)
/// \brief Gets the specified bot1 position.
/// \param *controller The Controller to use to do this function.
/// \return The position vector.
////////////////////////////////////////////////////
struct Vector3 SumoUY_ControllerGetBot1Position(struct SumoController *controller);

////////////////////////////////////////////////////
/// \fn struct Vector3 SumoUY_ControllerGetBot2Position(struct SumoController *controller)
/// \brief Gets the specified bot2 position.
/// \param *controller The Controller to use to do this function.
/// \return The position vector.
////////////////////////////////////////////////////
struct Vector3 SumoUY_ControllerGetBot2Position(struct SumoController *controller);

////////////////////////////////////////////////////
/// \fn struct SpaceRotation SumoUY_ControllerGetBot1Rotation(struct SumoController *controller)
/// \brief Gets the specified bot1 rotation.
/// \param *controller The Controller to use to do this function.
/// \return The space rotation.
////////////////////////////////////////////////////
struct SpaceRotation SumoUY_ControllerGetBot1Rotation(struct SumoController *controller);

////////////////////////////////////////////////////
/// \fn struct SpaceRotation SumoUY_ControllerGetBot2Rotation(struct SumoController *controller)
/// \brief Gets the specified bot2 rotation.
/// \param *controller The Controller to use to do this function.
/// \return The space rotation.
////////////////////////////////////////////////////
struct SpaceRotation SumoUY_ControllerGetBot2Rotation(struct SumoController *controller);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerSetBot1Position(struct SumoController *controller, struct Vector3 position)
/// \brief Sets the specified bot1 position.
/// \param *controller The Controller to use to do this function.
/// \param *position The position to set.
/// \return SUMOUY_TRUE if sucess and SUMOUY_FALSE on failure.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerSetBot1Position(struct SumoController *controller, struct Vector3 position);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerSetBot2Position(struct SumoController *controller, struct Vector3 position)
/// \brief Sets the specified bot2 position.
/// \param *controller The Controller to use to do this function.
/// \param *position The position to set.
/// \return SUMOUY_TRUE if sucess and SUMOUY_FALSE on failure.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerSetBot2Position(struct SumoController *controller, struct Vector3 position);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerSetBot1Rotation(struct SumoController *controller, struct SpaceRotation rotation)
/// \brief Sets the specified bot1 rotation.
/// \param *controller The Controller to use to do this function.
/// \param *rotation The rotation to set.
/// \return SUMOUY_TRUE if sucess and SUMOUY_FALSE on failure.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerSetBot1Rotation(struct SumoController *controller, struct SpaceRotation rotation);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerSetBot2Rotation(struct SumoController *controller, struct SpaceRotation rotation)
/// \brief Sets the specified bot2 rotation.
/// \param *controller The Controller to use to do this function.
/// \param *rotation The rotation to set.
/// \return SUMOUY_TRUE if sucess and SUMOUY_FALSE on failure.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerSetBot2Rotation(struct SumoController *controller, struct SpaceRotation rotation);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerOnBotSpeedSet(struct SumoController *controller, void (*onBotSpeedSet)(SumoUY_UInt8 bot, struct BotSpeed speed))
/// \brief Specifies the method to call when the Sumo|SERVER sends a bot speed data.
/// \param *controller The Controller to use to do this function.
/// \param *onBotSpeedSet The speed set function to call when the Sumo|SERVER sends the bot speed data.
/// \return SUMOUY_TRUE if sucess and SUMOUY_FALSE on failure.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerOnBotSpeedSet(struct SumoController *controller, void (*onBotSpeedSet)(SumoUY_UInt8 bot, struct BotSpeed speed));

//***************************************************************************************//
//Utility functions                                                                      //
//***************************************************************************************//

////////////////////////////////////////////////////
/// \fn struct Vector2 SumoUY_CreateVector2(SumoUY_Int32 x, SumoUY_Int32 y)
/// \brief Creates a vector with 2 dimensions of specified parameters.
/// \param x X position of the vector.
/// \param y Y position of the vector.
/// \return The created vector.
////////////////////////////////////////////////////
struct Vector2 SumoUY_CreateVector2(SumoUY_Int32 x, SumoUY_Int32 y);

////////////////////////////////////////////////////
/// \fn struct Vector3 SumoUY_CreateVector3(SumoUY_Float x, SumoUY_Float y, SumoUY_Float z)
/// \brief Creates a vector with 3 dimensions of specified parameters.
/// \param x X position of the vector.
/// \param y Y position of the vector.
/// \param y Z position of the vector.
/// \return The created vector.
////////////////////////////////////////////////////
struct Vector3 SumoUY_CreateVector3(SumoUY_Int32 x, SumoUY_Int32 y, SumoUY_Int32 z);

////////////////////////////////////////////////////
/// \fn struct SpaceRotation SumoUY_CreateSpaceRotation(SumoUY_Float x, SumoUY_Float y, SumoUY_Float z)
/// \brief Creates a Spatial Rotation of specified parameters.
/// \param x The X axis rotation.
/// \param y The Y axis rotation.
/// \param z The Z axis rotation.
/// \return The created spatial rotation.
////////////////////////////////////////////////////
struct SpaceRotation SumoUY_CreateSpaceRotation(SumoUY_UInt16 x, SumoUY_UInt16 y, SumoUY_UInt16 z);

////////////////////////////////////////////////////
/// \fn struct BotSpeed SumoUY_CreateBotSpeed(SumoUY_Int8 leftWheelSpeed, SumoUY_Int8 rightWheelSpeed)
/// \brief Creates a BotSpeed of specified parameters.
/// \param leftWheelSpeed The left wheel speed.
/// \param rightWheelSpeed The right wheel speed.
/// \return The created speed.
////////////////////////////////////////////////////
struct BotSpeed SumoUY_CreateBotSpeed(SumoUY_Int8 leftWheelSpeed, SumoUY_Int8 rightWheelSpeed);

////////////////////////////////////////////////////
/// \fn struct NetworkAddress SumoUY_CreateNetworkAddress(char *host, SumoUY_UInt16 port)
/// \brief Creates a NetworkAddress of specified parameters.
/// \param host The hostname.
/// \param port The UDP port.
/// \return The created network address.
////////////////////////////////////////////////////
struct NetworkAddress SumoUY_CreateNetworkAddress(char *host, SumoUY_UInt16 port);

////////////////////////////////////////////////////
/// \fn struct FightTime SumoUY_CreateFightTime(SumoUY_UInt8 hours, SumoUY_UInt8 minutes, SumoUY_UInt8 seconds)
/// \brief Creates a FightTime of specified parameters.
/// \param hours The hours.
/// \param minutes The minutes.
/// \param seconds The seconds.
/// \return The created fight time.
////////////////////////////////////////////////////
struct FightTime SumoUY_CreateFightTime(SumoUY_UInt8 hours, SumoUY_UInt8 minutes, SumoUY_UInt8 seconds);

////////////////////////////////////////////////////
/// \fn SumoUY_Int8 SumoUY_CompareFightTimes(struct FightTime time1, struct FightTime time2)
/// \brief Compares two Fight Times.
/// \param time1 A FightTime structure.
/// \param time2 A FightTime structure.
/// \return SUMOUY_EQUAL if the two fight times are equal, SUMOUY_BIGGER if time1 > time2 and SUMOUY_SMALLER if time1 < time2.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_CompareFightTimes(struct FightTime time1, struct FightTime time2);

//***************************************************************************************//
//Internal utility functions                                                             //
//***************************************************************************************//

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotSendData(struct SumoBot *bot,GString *data)
/// \brief Sends RAW data to the Sumo|SERVER.
/// \details If fails sets the SUMOUY_CANT_SEND_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \param data The data to send.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotSendData(struct SumoBot *bot,GString *data);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_BotRecieveData(struct SumoBot *bot, GString *data)
/// \brief Recieves RAW data from the Sumo|SERVER.
/// \details If fails sets the SUMOUY_CANT_RECIEVE_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *bot The SumoBot to use to do this function.
/// \param data A pointer to allocate the data recieved.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_BotRecieveData(struct SumoBot *bot, GString *data);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerSendData(struct SumoController *controller, GString data)
/// \brief Sends RAW data to the Sumo|SERVER.
/// \details If fails sets the SUMOUY_CANT_SEND_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *controller The Controller to use to do this function.
/// \param data The data to send.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerSendData(struct SumoController *controller, GString *data);

////////////////////////////////////////////////////
/// \fn SumoUY_UInt8 SumoUY_ControllerRecieveData(struct SumoController *controller, GString data)
/// \brief Recieves RAW data from the Sumo|SERVER.
/// \details If fails sets the SUMOUY_CANT_SEND_DATA or SUMOUY_INVALID_ARGUMENT error flag.
/// \param *controller The Controller to use to do this function.
/// \param data A pointer to allocate the data recieved.
/// \return SUMOUY_TRUE on sucess and SUMOUY_FALSE on fails.
////////////////////////////////////////////////////
SumoUY_UInt8 SumoUY_ControllerRecieveData(struct SumoController *controller, GString *data);
