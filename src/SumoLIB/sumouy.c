/*
    Sumo|LIB - A library for use in the Sumo|UY robotic fight events.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Sumo|LIB.

    Sumo|LIB is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Sumo|LIB (2009 - Steven Rodriguez -)                                 //
//=====================================================================//

//===========================================================//
//Libraries                                                  //
//===========================================================//
#include "sumouy.h"

//===========================================================//
//Global variables                                           //
//===========================================================//
static SumoUY_UInt8 currenterror = SUMOUY_NO_ERROR;
static SumoUY_UInt8 initialized = SUMOUY_FALSE;

//===========================================================//
//Functions                                                  //
//===========================================================//

//******************************************************//
//General library functions                             //
//******************************************************//

//Checked!!!
SumoUY_UInt8 SumoUY_Init()
{
	//Set the current error
	SumoUY_SetError(SUMOUY_NO_ERROR);

	//Initialize the system and verify if it was initialized
	if(SDLNet_Init() == -1 || initialized == SUMOUY_TRUE)
	{
		SumoUY_SetError(SUMOUY_COULD_NOT_INIT);
		initialized = SUMOUY_FALSE;

		return SUMOUY_FALSE;
	}

	initialized = SUMOUY_TRUE;

	return SUMOUY_TRUE;
}

//Checked!!!
void SumoUY_Close()
{
	//Close the system verifying if the system was initialized
	if(initialized == SUMOUY_TRUE)
	{
		SDLNet_Quit();
		initialized = SUMOUY_FALSE;
	}
}

//Checked!!!
SumoUY_UInt8 SumoUY_WasInit()
{
	return initialized;
}

//Checked!!!
SumoUY_UInt8 SumoUY_GetError()
{
	return currenterror;
}

//Checked!!!
char *SumoUY_GetErrorString()
{
    switch(currenterror)
    {
        case SUMOUY_CANT_CREATE_BOT:
            return "Can't create bot";
        case SUMOUY_COULD_NOT_INIT:
            return "Could not Init Sumo|LIB";
        case SUMOUY_INVALID_ARGUMENT:
            return "Invalid argument";
        case SUMOUY_CANT_RECIEVE_DATA:
            return "Can't recieve network data";
        case SUMOUY_CANT_SEND_DATA:
            return "Can't send network data";
        case SUMOUY_CANT_CREATE_SOCKET:
            return "Can't create socket";
        case SUMOUY_CANT_RESOLVE_HOST:
            return "Can't resolve host";
        case SUMOUY_CANT_SET_SPEED:
            return "Can't set speed";
        case SUMOUY_CANT_PROCESS_DATA:
            return "Can't process data";
        case SUMOUY_INVALID_DATA:
            return "Invalid data";
        case SUMOUY_NOT_INITIALIZED:
            return "Sumo|LIB is not initialized";
        case SUMOUY_NO_PACKET_TO_RECIEVE:
            return "There is no packet to recieve";
        case SUMOUY_CANT_CREATE_CONTROLLER:
            return "Can't create controller";
        default:
            return "There is no error";
    }
}

//Checked!!!
void SumoUY_SetError(SumoUY_UInt8 error)
{
	currenterror = error;
}

//Checked!!!
void SumoUY_ClearErrors()
{
	SumoUY_SetError(SUMOUY_NO_ERROR);
}

//Checked!!!
const char *SumoUY_GetString(SumoUY_UInt8 attribute)
{
	switch(attribute)
	{
		case SUMOUY_VERSION:
			return "0.3.0 alpha";
		case SUMOUY_VENDOR:
			return "Steven Rodriguez (stevencrc@digitecnology.zapto.org)";
		case SUMOUY_COMPILE_DATE:
			return __DATE__;
	}

	return NULL;
}

//***************************************************************************************//
//Bot-related functions                                                                  //
//***************************************************************************************//

//Checked!!!
struct SumoBot *SumoUY_CreateSumoBot(struct NetworkAddress serverAddress, struct NetworkAddress botAddress)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return NULL;
    }

    //Check if network addresses are not equal
    if((strcmp(serverAddress.Host, botAddress.Host) == 0) && (serverAddress.Port == botAddress.Port))
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return NULL;
    }

	//Declare bot
	struct SumoBot *bot = (struct SumoBot *)malloc(sizeof(struct SumoBot));

	//Check if can't declare bot
	if(bot == NULL)
	{
		SumoUY_SetError(SUMOUY_CANT_CREATE_BOT);
		return NULL;
	}

	//Set bot properties
	bot->ServerAddress = serverAddress;
	bot->BotAddress = botAddress;
	bot->Position = SumoUY_CreateVector2(0,0);
	bot->Rotation = 0;
	bot->Yukos = 0;
	bot->Speed = SumoUY_CreateBotSpeed(0,0);
	bot->OpponentPosition = SumoUY_CreateVector2(0,0);
	bot->OpponentRotation = 0;
	bot->OpponentYukos = 0;
	bot->OnUpdate = NULL;
	bot->OnReposition = NULL;
	bot->OnStartFight = NULL;
	bot->OnStopFight = NULL;

	//Create bot socket
	bot->Socket = (void *)SDLNet_UDP_Open(botAddress.Port);

	//Check if socket creation fails
	if(bot->Socket == NULL)
	{
		SumoUY_SetError(SUMOUY_CANT_CREATE_SOCKET);
		free(bot);
		return NULL;
	}

	//Initialize the address data pointer
	bot->AddressData = (void *)malloc(sizeof(IPaddress) * 2);

	//Add server IP address
	if(SDLNet_ResolveHost((IPaddress *)bot->AddressData, serverAddress.Host, serverAddress.Port) == -1)
    {
        SumoUY_SetError(SUMOUY_CANT_RESOLVE_HOST);
		SDLNet_UDP_Close((UDPsocket)bot->Socket);
        free(bot->AddressData);
        free(bot);
        return NULL;
    }

	//Add bot IP address
	if(SDLNet_ResolveHost(((IPaddress *)bot->AddressData) + 1, botAddress.Host, botAddress.Port) == -1)
    {
        SumoUY_SetError(SUMOUY_CANT_RESOLVE_HOST);
        SDLNet_UDP_Close((UDPsocket)bot->Socket);
        free(bot->AddressData);
        free(bot);
        return NULL;
    }

	return bot;
}

//Checked!!!
void SumoUY_DestroySumoBot(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return;
    }

	//Check if bot is valid
	if(bot == NULL)
	{
		SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
		return;
	}

	//Close the socket and free the packet data
	SDLNet_UDP_Close((UDPsocket)bot->Socket);

	//Free the resources
	free(bot->AddressData);
	free(bot);
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotSendOK(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	GString *message = g_string_new(NULL);

	g_string_append(message, "ok*");

	if(SumoUY_BotSendData(bot, message) == SUMOUY_FALSE)
	{
        g_string_free(message, TRUE);
        return SUMOUY_FALSE;
	}

	g_string_free(message, TRUE);

	return SUMOUY_TRUE;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotProcessData(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

    //Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Data variables
	GString *data = g_string_new(NULL);
    //Parse variables
	GString *arguments = g_string_new(NULL);
	char *point1 = NULL;
    char *point2 = NULL;
    GList *tokens = NULL;

	//Get the packets of the bot socket
	if(SumoUY_BotRecieveData(bot, data) == SUMOUY_FALSE)
	{
	    g_string_free(data, TRUE);
        g_string_free(arguments, TRUE);

	    if(SumoUY_GetError() == SUMOUY_NO_PACKET_TO_RECIEVE)
	    {
	        SumoUY_ClearErrors();
	        return SUMOUY_TRUE;
	    }

	    SumoUY_SetError(SUMOUY_CANT_PROCESS_DATA);
        return SUMOUY_FALSE;
	}

	//Check the type of data recieved
	//========================================================

	//Start
	if((data->len == 6) && (strncmp(data->str, "start*", 6) == 0))
	{
		//Call the method if exists
		if(bot->OnStartFight != NULL)
		{
			bot->OnStartFight();
			g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
		}

		return SUMOUY_TRUE;
	}
	else if((data->len == 5) && (strncmp(data->str, "stop*", 5) == 0)) //Stop
    {
        //Call the method if exists
        if(bot->OnStopFight != NULL)
        {
            bot->OnStopFight();
			g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
        }

        return SUMOUY_TRUE;
    }
	else if((data->len > 9) && (strncmp(data->str, "position*", 9) == 0)) //Reposition
    {
		struct Vector2 position;
		SumoUY_UInt16 rotation;

		g_string_append_printf(arguments, "%s", data->str);
		point1 = arguments->str;

        //Obtain tokens
		while((point2 = strchr(point1,'*')) != NULL)
        {
            tokens = g_list_append(tokens, (gpointer)point1);
            point2[0] = '\0';
            point2++;
            point1 = point2;
        }

        //Check for the number of tokens
        if(g_list_length(tokens) != 4)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse x position data
        if(sscanf((char *)g_list_nth_data(tokens,1), "%i", &position.X) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse y position data
        if(sscanf((char *)g_list_nth_data(tokens,2), "%i", &position.Y) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse rotation data
        if(sscanf((char *)g_list_nth_data(tokens,3), "%hu", &rotation) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

		//Call the method if exists
		if(bot->OnReposition != NULL)
            bot->OnReposition(position,rotation);

        g_string_free(data, TRUE);
        g_string_free(arguments, TRUE);
		if(tokens != NULL)
            g_list_free(tokens);

        return SUMOUY_TRUE;
	}
	else if((data->len > 7) && (strncmp(data->str, "update*", 7) == 0)) //Update
    {
        g_string_append_printf(arguments, "%s", data->str);
		point1 = arguments->str;

        //Obtain tokens
		while((point2 = strchr(point1,'*')) != NULL)
        {
            tokens = g_list_append(tokens, (gpointer)point1);
            point2[0] = '\0';
            point2++;
            point1 = point2;
        }

        //Check for the number of tokens
        if(g_list_length(tokens) != 9)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse x position data
        if(sscanf((char *)g_list_nth_data(tokens,1), "%i", &bot->Position.X) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse y position data
        if(sscanf((char *)g_list_nth_data(tokens,2), "%i", &bot->Position.Y) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse rotation data
        if(sscanf((char *)g_list_nth_data(tokens,3), "%hu", &bot->Rotation) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse yukos data
        if(sscanf((char *)g_list_nth_data(tokens,4), "%hhu", &bot->Yukos) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse x opponent position data
        if(sscanf((char *)g_list_nth_data(tokens,5), "%i", &bot->OpponentPosition.X) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse y opponent position data
        if(sscanf((char *)g_list_nth_data(tokens,6), "%i", &bot->OpponentPosition.Y) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
           return SUMOUY_FALSE;
        }

        //Parse opponent rotation data
        if(sscanf((char *)g_list_nth_data(tokens,7), "%hu", &bot->OpponentRotation) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

        //Parse opponent yukos data
        if(sscanf((char *)g_list_nth_data(tokens,8), "%hhu", &bot->OpponentYukos) != 1)
        {
            SumoUY_SetError(SUMOUY_INVALID_DATA);
            g_string_free(data, TRUE);
            g_string_free(arguments, TRUE);
            if(tokens != NULL)
                g_list_free(tokens);
            return SUMOUY_FALSE;
        }

		//Call the event method if exists
        if(bot->OnUpdate != NULL)
            bot->OnUpdate(bot->Position, bot->Rotation, bot->Yukos, bot->OpponentPosition, bot->OpponentRotation, bot->OpponentYukos);

        g_string_free(data, TRUE);
        g_string_free(arguments, TRUE);
        if(tokens != NULL)
            g_list_free(tokens);

        return SUMOUY_TRUE;
    }

	g_string_free(data, TRUE);
    g_string_free(arguments, TRUE);
    SumoUY_SetError(SUMOUY_INVALID_DATA);

    return SUMOUY_FALSE;
}

//******************************************************//
//Get/set functions                                     //
//******************************************************//

//Checked!!!
struct Vector2 SumoUY_BotGetPosition(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SumoUY_CreateVector2(0,0);
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SumoUY_CreateVector2(0,0);
    }

	return bot->Position;
}

//Checked!!!
SumoUY_UInt16 SumoUY_BotGetRotation(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return 0;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return 0;
    }

	return bot->Rotation;
}

//Checked!!!
struct BotSpeed SumoUY_BotGetSpeed(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SumoUY_CreateBotSpeed(0,0);
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SumoUY_CreateBotSpeed(0,0);
    }

	return bot->Speed;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotGetYukos(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return 0;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return 0;
    }

	return bot->Yukos;
}

//Checked!!!
struct Vector2 SumoUY_BotGetOpponentPosition(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SumoUY_CreateVector2(0,0);
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SumoUY_CreateVector2(0,0);
    }

	return bot->OpponentPosition;
}

//Checked!!!
SumoUY_UInt16 SumoUY_BotGetOpponentRotation(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return 0;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return 0;
    }

	return bot->OpponentRotation;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotGetOpponentYukos(struct SumoBot *bot)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return 0;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return 0;
    }

	return bot->OpponentYukos;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotSetSpeed(struct SumoBot *bot, struct BotSpeed speed)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

    //Check if bot is valid
	if(bot == NULL)
	{
		SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
		return SUMOUY_FALSE;
	}

    GString *message = g_string_new(NULL);

    //Check speed and bot parameters
    if(speed.LeftWheelSpeed < -5 || speed.LeftWheelSpeed > 5 || speed.RightWheelSpeed < -5 || speed.RightWheelSpeed > 5 || bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        g_string_free(message, TRUE);
        return SUMOUY_FALSE;
    }

	g_string_append_printf(message, "speed*%i*%i*", speed.LeftWheelSpeed, speed.RightWheelSpeed);

	if(SumoUY_BotSendData(bot, message) == SUMOUY_FALSE)
	{
	    SumoUY_SetError(SUMOUY_CANT_SET_SPEED);
        g_string_free(message, TRUE);
        return SUMOUY_FALSE;
	}

	g_string_free(message, TRUE);

	bot->Speed = speed;

	return SUMOUY_TRUE;
}

//******************************************************//
//Event-related functions                               //
//******************************************************//

//Checked!!!
SumoUY_UInt8 SumoUY_BotOnStartFight(struct SumoBot *bot, void (*onStartFight)())
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Check if handler is valid
    if(onStartFight == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    bot->OnStartFight = onStartFight;

    return SUMOUY_TRUE;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotOnStopFight(struct SumoBot *bot, void (*onStopFight)())
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Check if handler is valid
    if(onStopFight == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    bot->OnStopFight = onStopFight;

    return SUMOUY_TRUE;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotOnUpdate(struct SumoBot *bot, void (*onUpdate)(struct Vector2 position,SumoUY_UInt16 rotation, SumoUY_UInt8 yukos, struct Vector2 opponentPosition, SumoUY_UInt16 opponentRotation, SumoUY_UInt8 opponentYukos))
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Check if handler is valid
    if(onUpdate == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    bot->OnUpdate = onUpdate;

    return SUMOUY_TRUE;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotOnReposition(struct SumoBot *bot, void (*onReposition)(struct Vector2 position, SumoUY_UInt16 rotation))
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Check if bot is valid
    if(bot == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Check if handler is valid
    if(onReposition == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    bot->OnReposition = onReposition;

    return SUMOUY_TRUE;
}

//***************************************************************************************//
//Controller-related functions                                                           //
//***************************************************************************************//

struct SumoController *SumoUY_CreateSumoController(struct NetworkAddress serverAddress, struct NetworkAddress controllerAddress)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return NULL;
    }

    //Check if network addresses are not equal
    if((strcmp(serverAddress.Host, controllerAddress.Host) == 0) && (serverAddress.Port == controllerAddress.Port))
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return NULL;
    }

	//Declare controller
	struct SumoController *controller = (struct SumoController *)malloc(sizeof(struct SumoController));

	//Check if can't declare bot
	if(controller == NULL)
	{
		SumoUY_SetError(SUMOUY_CANT_CREATE_CONTROLLER);
		return NULL;
	}

	//Set controller properties
	controller->ServerAddress = serverAddress;
	controller->ControllerAddress = controllerAddress;
	controller->Bot1Position = SumoUY_CreateVector3(0, 0, 0);
	controller->Bot1Rotation = SumoUY_CreateSpaceRotation(0, 0, 0);
	controller->Bot2Position = SumoUY_CreateVector3(0, 0, 0);
	controller->Bot2Rotation = SumoUY_CreateSpaceRotation(0, 0, 0);
	controller->OnBotSpeedSet = NULL;

	//Create controller socket
	controller->Socket = (void *)SDLNet_UDP_Open(controllerAddress.Port);

	//Check if socket creation fails
	if(controller->Socket == NULL)
	{
		SumoUY_SetError(SUMOUY_CANT_CREATE_SOCKET);
		free(controller);
		return NULL;
	}

	//Initialize the address data pointer
	controller->AddressData = (void *)malloc(sizeof(IPaddress) * 2);

	//Add server IP address
	if(SDLNet_ResolveHost((IPaddress *)controller->AddressData, serverAddress.Host, serverAddress.Port) == -1)
    {
        SumoUY_SetError(SUMOUY_CANT_RESOLVE_HOST);
		SDLNet_UDP_Close((UDPsocket)controller->Socket);
        free(controller->AddressData);
        free(controller);
        return NULL;
    }

	//Add controller IP address
	if(SDLNet_ResolveHost(((IPaddress *)controller->AddressData) + 1, controllerAddress.Host, controllerAddress.Port) == -1)
    {
        SumoUY_SetError(SUMOUY_CANT_RESOLVE_HOST);
        SDLNet_UDP_Close((UDPsocket)controller->Socket);
        free(controller->AddressData);
        free(controller);
        return NULL;
    }

	return controller;
}

void SumoUY_DestroySumoController(struct SumoController *controller)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return;
    }

	//Check if controller is valid
	if(controller == NULL)
	{
		SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
		return;
	}

	//Close the socket and free the packet data
	SDLNet_UDP_Close((UDPsocket)controller->Socket);

	//Free the resources
	free(controller->AddressData);
	free(controller);
}

SumoUY_UInt8 SumoUY_ControllerProcessData(struct SumoController *controller)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

    //Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Data variables
	GString *data = g_string_new(NULL);
	SumoUY_UInt8 recieveddata = SUMOUY_TRUE;
	SumoUY_UInt8 invaliddata = SUMOUY_FALSE;
	struct BotSpeed speed;
	//Parse variables
	GString *arguments = g_string_new(NULL);
	char *point1 = NULL;
    char *point2 = NULL;
    GList *tokens = NULL;

	//Recieve data from Sumo|SERVER
	if(SumoUY_ControllerRecieveData(controller, data) == SUMOUY_FALSE)
	{
	    if(SumoUY_GetError() == SUMOUY_NO_PACKET_TO_RECIEVE)
	    {
	        recieveddata = SUMOUY_FALSE;
	        SumoUY_ClearErrors();
	    }
	    else
	    {
	        g_string_free(data, TRUE);
	        g_string_free(arguments, TRUE);
            SumoUY_SetError(SUMOUY_CANT_PROCESS_DATA);
            return SUMOUY_FALSE;
	    }
	}

	//Process data recieved from Sumo|SERVER
	if(recieveddata == SUMOUY_TRUE)
	{
	    if((data->len > 7) && (strncmp(data->str, "robot1*", 7) == 0)) //Set robot1 speed
        {
            g_string_append_printf(arguments, "%s", data->str);
            point1 = arguments->str;

            //Obtain tokens
            while((point2 = strchr(point1,'*')) != NULL)
            {
                tokens = g_list_append(tokens, (gpointer)point1);
                point2[0] = '\0';
                point2++;
                point1 = point2;
            }

            //Check for the number of tokens
            if(g_list_length(tokens) != 3)
            {
                invaliddata = SUMOUY_TRUE;
            }

            if(invaliddata == SUMOUY_FALSE)
            {
                //Parse left wheel speed
                if(sscanf((char *)g_list_nth_data(tokens,1), "%hhi", &speed.LeftWheelSpeed) != 1)
                {
                    invaliddata = SUMOUY_TRUE;
                }

                //Parse right wheel speed
                if(sscanf((char *)g_list_nth_data(tokens,2), "%hhi", &speed.RightWheelSpeed) != 1)
                {
                    invaliddata = SUMOUY_TRUE;
                }
            }

            //Call the method if exists
            if((controller->OnBotSpeedSet != NULL) && (invaliddata == SUMOUY_FALSE))
                controller->OnBotSpeedSet(SUMOUY_BOT1, speed);
        }
        else if((data->len > 7) && (strncmp(data->str, "robot2*", 7) == 0)) //Set robot2 speed
        {
            g_string_append_printf(arguments, "%s", data->str);
            point1 = arguments->str;

            //Obtain tokens
            while((point2 = strchr(point1,'*')) != NULL)
            {
                tokens = g_list_append(tokens, (gpointer)point1);
                point2[0] = '\0';
                point2++;
                point1 = point2;
            }

            //Check for the number of tokens
            if(g_list_length(tokens) != 3)
            {
                invaliddata = SUMOUY_TRUE;
            }

            if(invaliddata == SUMOUY_FALSE)
            {
                //Parse left wheel speed
                if(sscanf((char *)g_list_nth_data(tokens,1), "%hhi", &speed.LeftWheelSpeed) != 1)
                {
                    invaliddata = SUMOUY_TRUE;
                }

                //Parse right wheel speed
                if(sscanf((char *)g_list_nth_data(tokens,2), "%hhi", &speed.RightWheelSpeed) != 1)
                {
                    invaliddata = SUMOUY_TRUE;
                }
            }

            //Call the method if exists
            if((controller->OnBotSpeedSet != NULL) && (invaliddata == SUMOUY_FALSE))
                controller->OnBotSpeedSet(SUMOUY_BOT2, speed);
        }
        else
        {
            invaliddata = SUMOUY_TRUE;
        }
	}

	//Send data to Sumo|SERVER
	g_string_printf(data, "%i*%i*%i*%i*%i*%i*%i*%i*%i*%i*%i*%i*", controller->Bot1Position.X, controller->Bot1Position.Y, controller->Bot1Position.Z, controller->Bot1Rotation.X, controller->Bot1Rotation.Y, controller->Bot1Rotation.Z, controller->Bot2Position.X, controller->Bot2Position.Y, controller->Bot2Position.Z, controller->Bot2Rotation.X, controller->Bot2Rotation.Y, controller->Bot2Rotation.Z);

    if(SumoUY_ControllerSendData(controller, data) == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_CANT_PROCESS_DATA);
        g_string_free(data, TRUE);
        g_string_free(arguments, TRUE);
        if(tokens != NULL)
            g_list_free(tokens);
        return SUMOUY_FALSE;
    }

    //Free the resources
    g_string_free(data, TRUE);
    g_string_free(arguments, TRUE);
    if(tokens != NULL)
        g_list_free(tokens);

    //Check if recieved invalid data
    if(invaliddata == SUMOUY_TRUE)
    {
        SumoUY_SetError(SUMOUY_INVALID_DATA);
        return SUMOUY_FALSE;
    }

    return SUMOUY_TRUE;
}

//******************************************************//
//Get/set functions                                     //
//******************************************************//

struct Vector3 SumoUY_ControllerGetBot1Position(struct SumoController *controller)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SumoUY_CreateVector3(0, 0, 0);
    }

	//Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SumoUY_CreateVector3(0, 0, 0);
    }

	return controller->Bot1Position;
}

struct Vector3 SumoUY_ControllerGetBot2Position(struct SumoController *controller)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SumoUY_CreateVector3(0, 0, 0);
    }

	//Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SumoUY_CreateVector3(0, 0, 0);
    }

	return controller->Bot2Position;
}

struct SpaceRotation SumoUY_ControllerGetBot1Rotation(struct SumoController *controller)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SumoUY_CreateSpaceRotation(0, 0, 0);
    }

	//Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SumoUY_CreateSpaceRotation(0, 0, 0);
    }

	return controller->Bot1Rotation;
}

struct SpaceRotation SumoUY_ControllerGetBot2Rotation(struct SumoController *controller)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SumoUY_CreateSpaceRotation(0, 0, 0);
    }

	//Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SumoUY_CreateSpaceRotation(0, 0, 0);
    }

	return controller->Bot2Rotation;
}

SumoUY_UInt8 SumoUY_ControllerSetBot1Position(struct SumoController *controller, struct Vector3 position)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

    //Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Set the position
    controller->Bot1Position = position;

    return SUMOUY_TRUE;
}

SumoUY_UInt8 SumoUY_ControllerSetBot2Position(struct SumoController *controller, struct Vector3 position)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

    //Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Set the position
    controller->Bot2Position = position;

    return SUMOUY_TRUE;

}

SumoUY_UInt8 SumoUY_ControllerSetBot1Rotation(struct SumoController *controller, struct SpaceRotation rotation)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

    //Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Set the rotation
    controller->Bot1Rotation = rotation;

    return SUMOUY_TRUE;

}

SumoUY_UInt8 SumoUY_ControllerSetBot2Rotation(struct SumoController *controller, struct SpaceRotation rotation)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

    //Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Set the rotation
    controller->Bot2Rotation = rotation;

    return SUMOUY_TRUE;
}

//******************************************************//
//Event-related functions                               //
//******************************************************//

SumoUY_UInt8 SumoUY_ControllerOnBotSpeedSet(struct SumoController *controller, void (*onBotSpeedSet)(SumoUY_UInt8 bot, struct BotSpeed speed))
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Check if controller is valid
    if(controller == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    //Check if handler is valid
    if(onBotSpeedSet == NULL)
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

    controller->OnBotSpeedSet = onBotSpeedSet;

    return SUMOUY_TRUE;
}

//***************************************************************************************//
//Utility functions                                                                      //
//***************************************************************************************//

//Checked
struct Vector2 SumoUY_CreateVector2(SumoUY_Int32 x, SumoUY_Int32 y)
{
	struct Vector2 vector;

	//Set vector parameters
	vector.X = x;
	vector.Y = y;

	return vector;
}

struct Vector3 SumoUY_CreateVector3(SumoUY_Int32 x, SumoUY_Int32 y, SumoUY_Int32 z)
{
    struct Vector3 vector;

    //Set vector parameters
    vector.X = x;
    vector.Y = y;
    vector.Z = z;

    return vector;
}

struct SpaceRotation SumoUY_CreateSpaceRotation(SumoUY_UInt16 x, SumoUY_UInt16 y, SumoUY_UInt16 z)
{
    struct SpaceRotation rotation;

    //Set rotation parameters
    rotation.X = x;
    rotation.Y = y;
    rotation.Z = z;

    return rotation;
}

//Checked!!!
struct BotSpeed SumoUY_CreateBotSpeed(SumoUY_Int8 leftWheelSpeed, SumoUY_Int8 rightWheelSpeed)
{
	struct BotSpeed speed;

	//Set speed parameters
	speed.LeftWheelSpeed = leftWheelSpeed;
	speed.RightWheelSpeed = rightWheelSpeed;

	return speed;
}

//Checked!!!
struct NetworkAddress SumoUY_CreateNetworkAddress(char *host, SumoUY_UInt16 port)
{
	struct NetworkAddress address;

	//Set address parameters
	address.Host = host;
	address.Port = port;

	return address;
}

struct FightTime SumoUY_CreateFightTime(SumoUY_UInt8 hours, SumoUY_UInt8 minutes, SumoUY_UInt8 seconds)
{
    struct FightTime time;

    //Set time parameters
    time.Hours = hours;
    time.Minutes = minutes;
    time.Seconds = seconds;

    return time;
}

SumoUY_UInt8 SumoUY_CompareFightTimes(struct FightTime time1, struct FightTime time2)
{
    //Check if times are equal
    if((time1.Hours == time2.Hours) && (time1.Minutes == time2.Minutes) && (time1.Seconds == time2.Seconds))
        return SUMOUY_EQUAL;

    //Check if time1 > time2
    if((time1.Hours > time2.Hours) || ((time1.Hours == time2.Hours) && (time1.Minutes > time2.Minutes)) || ((time1.Hours == time2.Hours) && (time1.Minutes == time2.Minutes) && (time1.Seconds > time2.Seconds)))
        return SUMOUY_BIGGER;

    //Time1 < time2
    return SUMOUY_SMALLER;
}

//***************************************************************************************//
//Internal utility functions                                                             //
//***************************************************************************************//

//Checked!!!
SumoUY_UInt8 SumoUY_BotSendData(struct SumoBot *bot, GString *data)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Initializes the UDP packet
	UDPpacket *packet = SDLNet_AllocPacket(512);

	//Check if the packet was initialized
    if(packet == NULL)
	{
		SumoUY_SetError(SUMOUY_CANT_SEND_DATA);
		return SUMOUY_FALSE;
	}

	//Check if bot and data is valid
    if((bot == NULL) || (data == NULL))
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        SDLNet_FreePacket(packet);
        return SUMOUY_FALSE;
    }

    //Set the properties of the packet
	packet->address = *((IPaddress *)bot->AddressData);
    packet->data = (SumoUY_UInt8 *)data->str;
    packet->len = data->len;

	//Send the packet
	if(SDLNet_UDP_Send((UDPsocket)bot->Socket, -1, packet) == 0)
	{
	    //Little Hack: SDL_FreePacket frees the GString, generating a double_free corruption
        packet->data = NULL;
        packet->len = 0;

        //Free the packet resources
		SDLNet_FreePacket(packet);
		SumoUY_SetError(SUMOUY_CANT_SEND_DATA);
		return SUMOUY_FALSE;
	}

	//Little Hack: SDL_FreePacket frees the GString, generating a double_free corruption
	packet->data = NULL;
	packet->len = 0;

	//Free the packet resources
	SDLNet_FreePacket(packet);

	return SUMOUY_TRUE;
}

//Checked!!!
SumoUY_UInt8 SumoUY_BotRecieveData(struct SumoBot *bot, GString *data)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Initializes the packet
	UDPpacket *packet = SDLNet_AllocPacket(512);

	//Check if bot and data parameters are valid
    if((bot == NULL) || (data == NULL))
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

	//Check if the packet was initialized
	if(packet == NULL)
    {
        SumoUY_SetError(SUMOUY_CANT_RECIEVE_DATA);
        return SUMOUY_FALSE;
    }

    //Try to recieve the packet and check for errors
	if(SDLNet_UDP_Recv((UDPsocket)bot->Socket,packet) != 1)
	{
	    SDLNet_FreePacket(packet);
	    SumoUY_SetError(SUMOUY_NO_PACKET_TO_RECIEVE);
	    return SUMOUY_FALSE;
	}

	//Copy data to string
	g_string_append_len(data, (const gchar *)packet->data, packet->len);

	//Free the packet resources
	SDLNet_FreePacket(packet);

	return SUMOUY_TRUE;
}

SumoUY_UInt8 SumoUY_ControllerSendData(struct SumoController *controller, GString *data)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Initializes the UDP packet
	UDPpacket *packet = SDLNet_AllocPacket(512);

	//Check if the packet was initialized
    if(packet == NULL)
	{
		SumoUY_SetError(SUMOUY_CANT_SEND_DATA);
		return SUMOUY_FALSE;
	}

	//Check if controller is valid
    if((controller == NULL) || (data == NULL))
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        SDLNet_FreePacket(packet);
        return SUMOUY_FALSE;
    }

    //Set the properties of the packet
	packet->address = *((IPaddress *)controller->AddressData);
    packet->data = (SumoUY_UInt8 *)data->str;
    packet->len = data->len;

	//Send the packet
	if(SDLNet_UDP_Send((UDPsocket)controller->Socket, -1, packet) == 0)
	{
	    //Little Hack: SDL_FreePacket frees the GString, generating a double_free corruption
        packet->data = NULL;
        packet->len = 0;

        //Free the packet resources
		SDLNet_FreePacket(packet);
		SumoUY_SetError(SUMOUY_CANT_SEND_DATA);
		return SUMOUY_FALSE;
	}

	//Little Hack: SDL_FreePacket frees the GString, generating a double_free corruption
	packet->data = NULL;
	packet->len = 0;

	//Free the packet resources
	SDLNet_FreePacket(packet);

	return SUMOUY_TRUE;
}

SumoUY_UInt8 SumoUY_ControllerRecieveData(struct SumoController *controller, GString *data)
{
    //Check if Sumo|LIB is initialized
    if(initialized == SUMOUY_FALSE)
    {
        SumoUY_SetError(SUMOUY_NOT_INITIALIZED);
        return SUMOUY_FALSE;
    }

	//Initializes the packet
	UDPpacket *packet = SDLNet_AllocPacket(512);

	//Check if controller and data parameters are valid
    if((controller == NULL) || (data == NULL))
    {
        SumoUY_SetError(SUMOUY_INVALID_ARGUMENT);
        return SUMOUY_FALSE;
    }

	//Check if the packet was initialized
	if(packet == NULL)
    {
        SumoUY_SetError(SUMOUY_CANT_RECIEVE_DATA);
        return SUMOUY_FALSE;
    }

    //Try to recieve the packet and check for errors
	if(SDLNet_UDP_Recv((UDPsocket)controller->Socket,packet) != 1)
	{
	    SDLNet_FreePacket(packet);
	    SumoUY_SetError(SUMOUY_NO_PACKET_TO_RECIEVE);
	    return SUMOUY_FALSE;
	}

	//Copy data to string
	g_string_append_len(data, (const gchar *)packet->data, packet->len);

	//Free the packet resources
	SDLNet_FreePacket(packet);

	return SUMOUY_TRUE;
}
