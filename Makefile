################################
#Makefile for Sumo|LIB
################################

CC = gcc
CC_FLAGS = -fPIC
SRC_DIR = src/SumoLIB/
LIB_DIR = lib/
PKG_CONFIG_DIR = /usr/lib/pkgconfig/
SDL_CONFIG = `sdl-config --cflags --libs`
GLIB_CONFIG = `pkg-config --cflags --libs glib-2.0`
SYSTEM_HEADERS_DIR = /usr/include/
SYSTEM_LIBRARIES_DIR = /usr/lib/

all:
	#Compile library
	echo "Compiling Sumo|LIB..."
	$(CC) $(SDL_CONFIG) -c $(CC_FLAGS) -o $(LIB_DIR)sumouy.o -I $(SRC_DIR) $(GLIB_CONFIG) $(SRC_DIR)sumouy.c
	$(CC) -shared -lSDL_net $(CC_FLAGS) -o $(LIB_DIR)libsumouy.so $(LIB_DIR)sumouy.o

	#Remove .o thrash
	echo "Removing thrash..."
	rm $(LIB_DIR)sumouy.o
install:
	#Install headers
	echo "Making directory for Sumo|LIB system headers if not exists..."
	mkdir -p $(SYSTEM_HEADERS_DIR)sumouy/
	cp $(SRC_DIR)sumouy.h $(SYSTEM_HEADERS_DIR)sumouy/

	#Install library
	echo "Installing libsumouy..."
	cp $(LIB_DIR)libsumouy.so $(SYSTEM_LIBRARIES_DIR)libsumouy.so 
	
	#Install pkgconfig pc file
	echo "Installing pkg-config pc file..."
	cp pkgconfig/sumouy.pc $(PKG_CONFIG_DIR)sumouy.pc
uninstall:
	#Remove the components
	echo "Removing Sumo|LIB..."
	rm $(SYSTEM_LIBRARIES_DIR)libsumouy.so
	echo "Removing Sumo|LIB header..."
	rm $(SYSTEM_HEADERS_DIR)sumouy/sumouy.h
	echo "Removing directories created in installation..."
	rmdir $(SYSTEM_HEADERS_DIR)sumouy/
	echo "Removing pkg-config pc file..."
	rm $(PKG_CONFIG_DIR)sumouy.pc
clean:
	#Cleans compiling thrash
	echo "Cleaning thrash..."
	rm -f $(LIB_DIR)sumouy.o
	rm -f $(LIB_DIR)libsumouy.so
